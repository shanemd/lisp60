# lisp60
a C implementation of the original lisp. Inspired by Paul Graham's paper The Roots of Lisp

## Warning
It leaks memory like a sieve.

## How to ...
### Build
```
make
```

### Run the tests
```
make test
```

### Fire up the repl
```
make repl
```
