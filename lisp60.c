#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>

struct next_token
{
  const char* next;
  const char* rest;
};

const char* trim_left(const char* s)
{
  if (s[0] == ' ') return trim_left(s+1);
  else return s;
}

unsigned int find(const char *haystack, char needle)
{
  if (haystack[0] == needle ||
      haystack[0] == 0) return 0;
  else return 1 + find(haystack+1, needle);
}

bool contains(const char* str, char c)
{
  return find(str, c) < strlen(str);
}

unsigned int find1(const char* haystack,
		   const char* needles)
{
  if (haystack[0] == 0 ||
      contains(needles, haystack[0])) return 0;
  else return 1 + find1(haystack+1, needles);
}

const struct next_token get_next_token(const char* str)
{
  //while (str[0] == ' ') str++;
  const char* trimmed = trim_left(str);

  const unsigned int tok_len =
    contains("'()", trimmed[0]) ? 1 :
    find1(trimmed, " '()");
  
  // NOTE: strndup leaks
  return (struct next_token)
    {.next = strndup(trimmed, tok_len),
     .rest = trimmed + tok_len};
}

void print_nt(const struct next_token nt) {
  printf("{.next = \"%s\", .rest = \"%s\"}\n", nt.next, nt.rest);
}

bool streq(const char* a, const char* b)
{
  return strcmp(a, b) == 0;
}

bool nt_equal(const struct next_token a,
	      const struct next_token b)
{
  return streq(a.next, b.next) &&
    streq(a.rest, b.rest);
}

typedef struct token_test
{
  const char *name;
  const char *input;
  const struct next_token expected_output;
} token_test;

void run_token_test(const token_test test)
{
  const struct next_token actual =
    get_next_token(test.input);
  
  if (!nt_equal(actual, test.expected_output)) {
    printf("Failed Test: %s\n", test.name);
    printf("  Input: \"%s\"\n", test.input);
    printf("  Expected:");
    print_nt(test.expected_output);
    printf("  Got:");
    print_nt(actual);

    assert(false);
  } else {
    //printf("Passed: %s\n", test.name);
  }
}

const token_test token_tests[] = {
  {"it extracts a lone open paren",
   .input = "(", .expected_output = {"(", ""}},
  {"it extracts open paren after leading space",
   .input = " (", .expected_output = {"(", ""}},
  {"it extracts open paren after many leading spaces",
   .input = "   (", .expected_output = {"(", ""}},
  {"it extracts a lone multi-character symbol",
   .input = "foo", .expected_output = {"foo", ""}},
  {"it extracts a multi-character symbol after spaces",
   .input = "   foo", .expected_output = {"foo", ""}},
  {"it extracts a symbol with trailing space",
   .input = "foo ", .expected_output = {"foo", " "}},
  {"it extracts an open paren before a symbol",
   .input = "(foo)", .expected_output = {"(", "foo)"}},
  {"it extracts a symbol before a close paren",
   .input = "foo)", .expected_output = {"foo", ")"}},
  {"it extracts a symbol before an open paren",
   .input = "foo(", .expected_output = {"foo", "("}},
  {"it extracts a close paren before a symbol",
   .input = ")foo", .expected_output = {")", "foo"}},
  
  {"it extracts a lone quote",
   .input = "'", .expected_output = {"'", ""}},
  {"it extracts a lone quote before a symbol",
   .input = "' foo", .expected_output = {"'", " foo"}},
  {"it extracts a lone quote before a paren",
   .input = "' (", .expected_output = {"'", " ("}},
  {"it extracts a quote before a symbol",
   .input = "'foo", .expected_output = {"'", "foo"}},
  {"it extracts a quote before a paren",
   .input = "'(", .expected_output = {"'", "("}},
  
};

void run_token_tests(const token_test* tests,
		     unsigned int num_tests)
{
  if (num_tests == 0) return;
  
  run_token_test(tests[0]);
  run_token_tests(tests+1, num_tests-1);
}

#define ARRAY_LEN(x) (sizeof(x) / sizeof(x[0]))

typedef struct string_array string_array;
struct string_array
{
  const char** strings;
  const unsigned int nstrings;
};

// NOTE: this leaks memory with calloc
const struct string_array sa_append(const struct string_array a, const char* str)
{
  const char** new_array = calloc(a.nstrings+1,
				  sizeof(char*));
  const unsigned int new_size = a.nstrings+1;
  memcpy(new_array, a.strings, a.nstrings * sizeof(char*));

  new_array[new_size-1] = str;

  return (const struct string_array)
    {.strings = new_array,
     .nstrings = new_size};
}

void print_sa(const struct string_array a)
{
  printf("[");
  for (int i = 0; i < a.nstrings; i++) {
    printf("\"%s\"", a.strings[i]);
    if (i < a.nstrings-1) printf(", ");
  }
  printf("]");
}

unsigned int len(const string_array sa)
{
  return sa.nstrings;
}

const char* first(const string_array sa)
{
  return sa.strings[0];
}

const string_array drop_first(const string_array sa)
{
  return (string_array){sa.strings+1, len(sa)-1};
}

bool sa_equal(const string_array a,
	      const string_array b)
{
  if (len(a) != len(b)) return false;
  if (len(a) == 0) return true;
  if (!streq(first(a), first(b))) return false;

  return sa_equal(drop_first(a), drop_first(b));
}

const struct string_array tokenize_step(const struct string_array so_far,
					const char *str)
{
  if (strlen(trim_left(str)) == 0) return so_far;
  else {
    const struct next_token nt = get_next_token(str);
    return
      tokenize_step(sa_append(so_far, nt.next), nt.rest);
  }
}

const struct string_array tokenize(const char* str)
{
  return tokenize_step((struct string_array){0}, str);
}

typedef struct expr expr;
typedef enum expr_type expr_type;

enum expr_type { NIL = 0, CONS, SYMBOL };

struct expr
{
  const expr_type type;
  union
  {
    const expr* cons[2];
    const char* symbol_name;
  } u;
};

bool iscons(const expr* e)
{
  return e->type == CONS;
}


bool isnil(const expr* e)
{
  return e->type == NIL;
}

bool islist(const expr* e)
{
  return isnil(e) || iscons(e);
}

const expr* car(const expr* e)
{
  assert(iscons(e));

  return e->u.cons[0];
}

const expr* cdr(const expr* e)
{
  assert(iscons(e));

  return e->u.cons[1];
}

bool expr_equal(const expr* a, const expr* b)
{
  if (a->type != b->type) return false;

  if (a->type == NIL)
    return true;
  if (a->type == SYMBOL)
    return streq(a->u.symbol_name, b->u.symbol_name);
  if (a->type == CONS) {
    return expr_equal(car(a), car(b)) &&
      expr_equal(cdr(a), cdr(b));
  }
  
  return false;
}

unsigned int listlen(const expr* list)
{
  assert(islist(list));

  if (isnil(list))
    return 0;
  else
    return 1 + listlen(cdr(list));
}

void print_expr(const expr* e)
{
  if (e->type == NIL)
    printf("nil");
  if (e->type == SYMBOL)
    printf("(sym '%s')", e->u.symbol_name);
  if (e->type == CONS) {
    printf("(cons ");
    print_expr(e->u.cons[0]);
    printf(" ");
    print_expr(e->u.cons[1]);
    printf(")");
  }
}

void princ(const expr* e);

void princ_list_internal(const expr* list)
{
  assert(islist(list));

  if (isnil(list))
    return;
  else {
    princ(car(list));

    if (listlen(list) == 1)
      return;
    else {
      printf(" ");
      princ_list_internal(cdr(list));
    } 
  }
}

void princ(const expr* e)
{
  if (e->type == NIL)
    printf("nil");
  if (e->type == SYMBOL)
    printf("%s", e->u.symbol_name);
  if (e->type == CONS) {
    printf("(");
    princ_list_internal(e);
    printf(")");
  }
}


const expr nil_v = {.type = NIL};
const expr* nil = &nil_v;

const expr *cons(const expr *a, const expr *d)
{
  expr* c = calloc(1, sizeof(expr));

  const expr new_expr = (expr){.type = CONS,
			       .u.cons = {a, d}};
  memcpy(c, &new_expr, sizeof(expr));

  return c;
}

const expr* symbol(const char* name)
{
  expr* c = calloc(1, sizeof(expr));

  const expr new_expr = (expr){.type = SYMBOL,
			       .u.symbol_name = name};
  memcpy(c, &new_expr, sizeof(expr));

  return c;
}

bool empty(const struct string_array a)
{
  return a.nstrings == 0;
}

typedef struct next_expr
{
  const expr* next;
  const string_array rest;
  
} next_expr;

bool ne_equal(const next_expr a, const next_expr b)
{
  return expr_equal(a.next, b.next) &&
    sa_equal(a.rest, b.rest);
}

void print_ne(const next_expr ne)
{
  printf("{.next=");
  princ(ne.next);
  printf(", rest=");
  print_sa(ne.rest);
  printf("}\n");
}

typedef struct parse_test
{
  const char* name;
  const struct string_array tokens_in;
  const next_expr expected_output;
  
} parse_test;

#define SA_LEN(...) (sizeof((const char*[]){__VA_ARGS__}) / sizeof(const char *))
#define SA(...) (string_array){(const char *[]){__VA_ARGS__}, SA_LEN(__VA_ARGS__)}

#define MAKE_NIL &(expr){.type=NIL}
#define SYM(x) &(expr){.type=SYMBOL, .u.symbol_name=x}
#define CON(a, b) &(expr){.type=CONS, .u.cons={a, b}}

const parse_test parse_tests[] = {
  {"it returns nil if no tokens", SA(),
   {.next=MAKE_NIL, .rest=SA()}},
  {"it extracts single symbol", SA("foo"),
   {.next=SYM("foo"), .rest=SA()}},
  {"it extracts one empty list as nil", SA("(", ")"),
   {.next=MAKE_NIL, .rest=SA()}},
  {"it extracts a one-element list",
   SA("(", "foo", ")"),
   {.next=CON(SYM("foo"), MAKE_NIL), .rest=SA()}},
  {"it extracts a many-element list",
   SA("(", "foo", "bar", "baz", ")"),
   {.next=CON(SYM("foo"),
	      CON(SYM("bar"),
		  CON(SYM("baz"),
		      MAKE_NIL))),
    .rest=SA()}},
  {"it extracts a nested list",
   SA("(", "(", "foo", ")", ")"),
   {.next=CON(CON(SYM("foo"),
		  MAKE_NIL),
	      MAKE_NIL),
    .rest=SA()}},

  // For completeness
  {"it leaves tokens following a symbol in rest",
   SA("foo", "bar"),
   {.next=SYM("foo"), .rest=SA("bar")}},
  {"it leaves tokens following a list in rest",
   SA("(", "foo", ")", "bar"),
   {.next=CON(SYM("foo"), MAKE_NIL),
    .rest=SA("bar")}},

  {"it extracts nil specially",
   SA("nil"),
   {.next=MAKE_NIL, .rest=SA()}},

  {"it quotes a symbol",
   SA("'", "foo"),
   {.next=CON(SYM("quote"),
	      CON(SYM("foo"),
		  MAKE_NIL))}},
  {"it quotes an empty list",
   SA("'", "(", ")"),
   {.next=CON(SYM("quote"),
	      CON(MAKE_NIL,
		  MAKE_NIL))}},
  {"it quotes a list with an element",
   SA("'", "(", "foo", ")"),
   {.next=CON(SYM("quote"),
	      CON(CON(SYM("foo"),
		      MAKE_NIL),
		  MAKE_NIL))}},
  {"it quotes a list with multiple elements",
   SA("'", "(", "foo", "bar", "baz", ")"),
   {.next=CON(SYM("quote"),
	      CON(CON(SYM("foo"),
		      CON(SYM("bar"),
			  CON(SYM("baz"),
			      MAKE_NIL))),
		  MAKE_NIL))}},
};

const char* second(const string_array sa)
{
  return first(drop_first(sa));
}

const string_array dropn(unsigned int n,
			 const string_array sa)
{
  if (n == 0) return sa;
  else {
    assert(len(sa) > 0);
    return dropn(n-1, drop_first(sa));
  }
}

const char* nth(unsigned int n,
		const string_array sa)
{
  assert(n > 0);
  assert(len(sa) >= n);
    
  if (n == 1) return first(sa);
  else return nth(n-1, drop_first(sa));
}

typedef const expr* fold_fn_t(const expr*,
			      const expr*);

const expr* foldl(fold_fn_t* fn,
		  const expr* list,
		  const expr* right)
{
  if (isnil(list))
    return right;
  else
    return foldl(fn, cdr(list), fn(car(list), right));
}

const expr* reverse(const expr* list)
{
  assert(islist(list));

  return foldl(cons, list, nil);
}

const next_expr parse_next(const
			   struct string_array tokens);

const next_expr
parse_list(const struct string_array tokens,
	   const expr* list_so_far)
{
  if (len(tokens) == 0)
    return (next_expr){.next = reverse(list_so_far),
		       .rest = tokens};
  if (streq(first(tokens), ")"))
    return (next_expr){.next = reverse(list_so_far),
		       .rest = drop_first(tokens)};
  else {
    const next_expr next = parse_next(tokens);
    return parse_list(next.rest,
		      cons(next.next,
			   list_so_far));
  }
  
  return (next_expr){nil, tokens};
}

const next_expr parse_next(const
			   struct string_array tokens)
{
  if (len(tokens) == 0)
    return (next_expr){nil, tokens};
  else if (len(tokens) >= 2 &&
	   streq(first(tokens), "(") &&
	   streq(second(tokens), ")"))
    return (next_expr)
      {.next = nil,
       .rest = drop_first(drop_first(tokens))};
  else if (streq(first(tokens), "("))
    return parse_list(drop_first(tokens), nil);
  else if (streq(first(tokens), "nil"))
    return (next_expr){.next = nil,
		       .rest = drop_first(tokens)};
  else if (streq(first(tokens), "'")) {
    const next_expr next = parse_next(drop_first(tokens));
    return (next_expr) {.next = cons(symbol("quote"),
				     cons(next.next,
					  nil)),
			.rest = next.rest};
  } else
    return (next_expr){.next = symbol(first(tokens)),
		       .rest = drop_first(tokens)};
}

void run_parse_test(const parse_test test)
{
  const next_expr actual = parse_next(test.tokens_in);

  if (!ne_equal(actual, test.expected_output)) {
    printf("Test failed: %s\n", test.name);
    printf("Input:\n");
    print_sa(test.tokens_in);
    printf("\n");
    printf("Expected:\n");
    print_ne(test.expected_output);
    printf("Got:\n");
    print_ne(actual);
    
    assert(false);
  }
}

void run_parse_tests(const parse_test* tests,
		     unsigned int ntests)
{
  for (int i = 0; i < ntests; i++)
    run_parse_test(tests[i]);
}

const expr* parse(const string_array tokens)
{
  const next_expr ne = parse_list(tokens, nil);

  assert(len(ne.rest) == 0);

  if (listlen(ne.next) == 1)
    return car(ne.next);
  else
    return ne.next;
}

#include <readline/readline.h>
#include <readline/history.h>

void rp()
{
  // Read
  const char* line = readline("LISP60> ");
  const string_array tokens = tokenize(line);
  const expr* e = parse(tokens);

  princ(e);
  printf("\n");
}

const expr* quote(const expr* e)
{
  return e;
}

const expr* t = SYM("t");

bool isatom(const expr* e)
{
  return !iscons(e);
}

#define WRAP_BOOL(x) (x ? t : nil)

const expr* atom (const expr* e)
{
  return WRAP_BOOL(isatom(e));
}

bool issymbol(const expr* e)
{
  return e->type == SYMBOL;
}

const char* symbol_name(const expr* e)
{
  assert(issymbol(e));

  return e->u.symbol_name;
}


// Wouldn't need this if we had a guarantee that only one symbol
// existed in memory for each name
bool symbol_equal(const expr* a, const expr* b)
{
  assert(issymbol(a));
  assert(issymbol(b));
  
  return streq(symbol_name(a), symbol_name(b));
}

bool iseq(const expr* a, const expr* b)
{
  // This could be as simple as:
  //   return a == b;
  // if we had guarantees about each symbol only
  // existing once in memory.
  
  if (isnil(a) && isnil(b))
    return true;
  else if (issymbol(a) && issymbol(b) &&
	   symbol_equal(a, b))
    return true;
  else
    return false;
}

const expr* eq(const expr* a, const expr* b)
{
  return WRAP_BOOL(iseq(a, b));
}

const expr* cadr(const expr* e)
{
  return car(cdr(e));
}

typedef struct eval_test
{
  const char* name;
  const char* in;
  const char* expected_out;
  const expr* env;
} eval_test;

const eval_test eval_tests[] = {
  {"nil evals to nil", "nil", "nil"},
  {"t evals to t", "t", "t"},
  
  {"(quote a) evals to a",
   "(quote a)", "a"},
  {"'a evals to a",
   "'a", "a"},
  
  {"a is an atom",
   "(atom 'a)", "t"},
  {"(a b) is not an atom",
   "(atom '(a b))", "nil"},
  
  {"same symbols are eq",
   "(eq 'a 'a)", "t"},
  {"different symbols are not eq",
   "(eq 'a 'b)", "nil"},
  {"empty lists are eq",
   "(eq () ())", "t"},

  {"car returns first element of list",
   "(car '(a b c))", "a"},
  {"car returns first element of another list",
   "(car '(b c))", "b"},

  {"cdr returns all but first element of list",
   "(cdr '(a b c))", "(b c)"},
  {"cdr returns all but first element of other list",
   "(cdr '(b c))", "(c)"},

  {"cons adds an element to a list",
   "(cons 'a '(b c))", "(a b c)"},
  {"cons adds a list to a list",
   "(cons '(c b) '(a))", "((c b) a)"},
  {"cons builds lists incrementally",
   "(cons 'a (cons 'b (cons 'c '())))", "(a b c)"},
  
  {"car can be used directly after cons",
   "(car (cons 'a '(b c)))", "a"},
  {"cdr can be used directly after cons",
   "(cdr (cons 'a '(b c)))", "(b c)"},

  {"cond evals to the value of the second expr if the first expr is non-nil",
   "(cond (t 'a))", "a"},
  {"cond falls through to the second pair if the first evals nil",
   "(cond ((eq 'a 'b) 'first) ((atom 'a) 'second))",
   "second"},
  {"cond evals to nil if no pairs eval non-nil",
   "(cond (nil 'a) ((eq 'a nil) nil))",
   "nil"},
  {"cond evals to nil if no pairs are given",
   "(cond)", "nil"},

  {"it can eval lambda exprs",
   "((lambda () 't))", "t"},
  {"it can actually eval lambda exprs",
   "((lambda () 'a))", "a"},

  {"it can eval a symbol from the environment",
   "foo", "bar", .env = CON(CON(SYM("foo"),
				CON(SYM("bar"),
				    MAKE_NIL)),
			    MAKE_NIL)},
  {"it can actually eval a symbol from the environ",
   "foo", "baz", .env = CON(CON(SYM("foo"),
				CON(SYM("baz"),
				    MAKE_NIL)),
			    MAKE_NIL)},

  {"it can eval lambda exprs with one param",
   "((lambda (x) x) 'b)", "b"},
  {"it can actually eval lambda exprs with params",
   "((lambda (x) x) 'c)", "c"},
  {"it can eval lambda exprs with many params",
   "((lambda (x y z) (cons x (cons y (cons z nil))))\
      'a 'b 'c)",
   "(a b c)"},

  {"parameters can be functions",
   "((lambda (f) (f '(b c))) \
     '(lambda (x) (cons 'a x)))",
   "(a b c)"},

  {"label expressions allow recursion",
   "((label dropn \
      (lambda (x n) \
       (cond ((eq n nil) x) \
              ('t (dropn (cdr x) (cdr n)))))) \
      '(a b c) '(x x))",
   "(c))"},
  {"label expressions actually allow recursion",
   "((label dropn \
      (lambda (x n) \
       (cond ((eq n nil) x) \
              ('t (dropn (cdr x) (cdr n)))))) \
      '(a b c d e) '(x x x))",
   "(d e))"},

  {"label exprs from Paul Graham's paper work",
   "((label subst"
   "        (lambda (x y z)"
   "        (cond ((atom z) \
                   (cond ((eq z y) x) \
                          ('t z))) \
                   ('t \
                    (cons (subst x y (car z)) \
                          (subst x y (cdr z))))))) \
      'm 'b '(a b (a b c) d))",
   "(a m (a m c) d)"},
};

const expr* cddr(const expr* e)
{
  return cdr(cdr(e));
}

const expr* caddr(const expr* e)
{
  return car(cdr(cdr(e)));
}

const expr* caar(const expr* e)
{
  return car(car(e));
}

const expr* caddar(const expr* e)
{
  return car(cdr(cdr(car(e))));
}

const expr* cadar(const expr* e)
{
  return car(cdr(car(e)));
}

const expr* assoc(const expr* a, const expr* alist)
{
  if (isnil(alist))
    return nil;
  else if (iseq(a, caar(alist)))
    return cadar(alist);
  else
    return assoc(a, cdr(alist));
}

const expr* read_expr(const char* s)
{
  const string_array tokens = tokenize(s);
  return parse(tokens);
}

const expr* eval(const expr* e, const expr* env);

const expr* evcon(const expr* pairs, const expr* env)
{
  if (isnil(pairs)) {
    return nil;
  } else {
    const expr* pair = car(pairs);

    if (!isnil(eval(car(pair), env))) {
      return eval(cadr(pair), env);
    } else {
      return evcon(cdr(pairs), env);
    }
  }
}

const expr* evlis(const expr* list, const expr* env)
{
  if (isnil(list))
    return list;
  else
    return cons(eval(car(list), env),
		evlis(cdr(list), env));
}

const expr* pairs(const expr* as, const expr* bs)
{
  if (isnil(as) || isnil(bs))
    return nil;
  else
    return cons(cons(car(as),
		     cons(car(bs),
			  nil)),
		pairs(cdr(as), cdr(bs)));
}

const expr* append(const expr* x, const expr* y)
{
  if (isnil(x))
    return y;
  else
    return cons(car(x),
		append(cdr(x), y));
}

const expr* pair(const expr* a, const expr* b)
{
  return cons(a, cons(b, nil));
}

const expr* eval(const expr* e, const expr* env)
{
  /* printf("\neval("); */
  /* princ(e); */
  /* printf(",\n"); */
  /* princ(env); */
  /* printf(")\n"); */
  
  if (isatom(e)) {
    if (iseq(e, MAKE_NIL)) {

      return nil;
    
    } else if (iseq(e, t)) {

      return t;
      
    } else {

      return assoc(e, env);
      return read_expr("(error variableNotBound)");
      
    }
  } else if (isatom(car(e))) {
    if (iseq(car(e), SYM("quote"))) {

      return cadr(e);
    
    } else if (iseq(car(e), SYM("atom"))) {

      return atom(eval(cadr(e), env));
    
    } else if (iseq(car(e), SYM("eq"))) {

      const expr* a = eval(cadr(e), env);
      const expr* b = eval(caddr(e), env);
      return eq(a, b);
    
    } else if (iseq(car(e), SYM("car"))) {

      const expr* a = eval(cadr(e), env);
      return car(a);
    
    } else if (iseq(car(e), SYM("cdr"))) {

      const expr* a = eval(cadr(e), env);
      return cdr(a);
    
    } else if (iseq(car(e), SYM("cons"))) {
    
      const expr* a = eval(cadr(e), env);
      const expr* b = eval(caddr(e), env);
      return cons(a, b);
    
    } else if (iseq(car(e), SYM("cond"))) {
    
      return evcon(cdr(e), env);
    
    } else {
      
      return eval(cons(assoc(car(e), env),
		       cdr(e)),
		  env);
    }
  } else if (iseq(caar(e), SYM("lambda"))) {
    const expr* lambda = car(e);
    const expr* params = cadr(lambda);
    const expr* body = caddr(lambda);
    
    const expr* args = cdr(e);
    
    return eval(body,
		append(pairs(params,
			     evlis(args, env)),
		       env));  
  } else if (iseq(caar(e), SYM("label"))) {
    const expr* label = car(e);
    const expr* name = cadr(label);
    const expr* lambda = caddr(label);
    const expr* args = cdr(e);
    
    return eval(cons(lambda, args),
		cons(pair(name, lambda),
		     env));
  }

  printf("Fell through\n");
  
  return nil;
}

void run_eval_test(const eval_test test)
{
  const expr* env = test.env ? test.env : nil;
  const expr* actual = eval(parse(tokenize(test.in)),
			    env);
  const expr* expected =
    parse(tokenize(test.expected_out));
  
  if (!expr_equal(expected, actual)) {
    printf("Failed test: %s\n", test.name);
    printf("Expected: ");
    princ(expected);
    printf("\n");
    printf("Got: ");
    princ(actual);
    printf("\n");
    
    assert(false);
  }
}

void run_eval_tests(const eval_test tests[],
		    unsigned int ntests)
{
  if (ntests == 0)
    return;
  else {
    run_eval_test(tests[0]);
    return run_eval_tests(tests+1, ntests-1);
  }
}

void rep()
{
  // Read
  const char* line = readline("LISP60> ");

  add_history(line);
  
  const string_array tokens = tokenize(line);
  const expr* e = parse(tokens);
  
  princ(eval(e, nil));
  printf("\n");
}


int main (int argc, char* argv[])
{
  if (argc >= 2 && streq(argv[1], "--run-tests")) {
    run_token_tests(token_tests,
		    ARRAY_LEN(token_tests));
    run_parse_tests(parse_tests,
		    ARRAY_LEN(parse_tests));
    run_eval_tests(eval_tests,
		   ARRAY_LEN(eval_tests));
  
    printf("All tests pass.\n");
    
  } else {
    using_history();
    while (true) {
      rep();
    }
  }

  return 0;
}
